package neto.imageinfos;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ImageInfoTest extends TestCase {

    public static final Logger log = LoggerFactory.getLogger(ImageInfoTest.class);

    @Test
    public void testGetBitsPerPixel() {
        ImageInfo imgInfo = new ImageInfo();
        try {
            imgInfo.setInput(new BufferedInputStream(new FileInputStream(new File("images/Recibo Fisioterapia 2014-03-31 Eleonor.jpg"))));
            imgInfo.setDetermineImageNumber(true);
            Assert.assertTrue(imgInfo.check());
            Assert.assertEquals(24, imgInfo.getBitsPerPixel());
            Assert.assertEquals(200, imgInfo.getPhysicalWidthDpi());
            Assert.assertEquals(200, imgInfo.getPhysicalHeightDpi());
        } catch (FileNotFoundException e) {
            log.error(null, e);
            Assert.fail(e.toString());
        }
    }
}